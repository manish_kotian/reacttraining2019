import React,{ Component} from 'react';
import FirstCompo21 from './first_2_1_compo';
import FirstCompo22 from './first_2_2_compo';
class FirstCompo2 extends Component {
	
    
    render(){
        return (
        <div className="FirstCompo2">
                <FirstCompo21 />
                <div className="FirstCompo22">
                <FirstCompo22 />
                </div>
        </div>

        );
    }
}

export default FirstCompo2;