import React,{ Component} from 'react';
class FirstCompo112 extends Component {
    
    render(){
        return (
        <div className="FirstCompo112">
               <h1>{this.props.name}</h1>
               <p>{this.props.nicname}</p>
        </div>

        );
    }
}

export default FirstCompo112;