import React,{ Component} from 'react';
class FirstCompo12 extends Component {
    
    render(){
        return (
        <div className="FirstCompo12">
               <hr/>
               <div>
	               <div>
		               <h1>{this.props.followers}</h1>
		               <h6>followers</h6>
	               </div>
	               <div>
		               <h1>{this.props.repositories}</h1>
		               <h6>repositories</h6>
	               </div>
	               <div>
		               <h1>{this.props.following}</h1>
		               <h6>following</h6>
		           </div>
               </div>
               <hr/>
        </div>

        );
    }
}

export default FirstCompo12;