import React,{ Component} from 'react';
const ObjectTest = {
    1: {
        repository : 'first-react-app',
        updated:'Update 8 months ago',
        lang:'Objective-C',
        star:0,
        vay:0
    },
    2: {
        repository : 'code-review',
        updated:'Update a year ago',
        lang:'swift',
        star:0,
        vay:0
    },
    3: {
        repository : 'mobile-vasunandi',
        updated:'Update a year ago',
        lang:'java',
        star:0,
        vay:0
    },
    4: {
        repository : 'backend-vasunandi',
        updated:'Update a year ago',
        lang:'python',
        star:0,
        vay:0
    },
    5: {
        repository : 'learning-area',
        updated:'Github repo for the MDN Learning Area',
        lang:'swift',
        star:0,
        vay:0
    }
}
class FirstCompo22 extends Component {
    
	_renderObject(){
		return Object.entries(ObjectTest).map(([key, value], i) => {
			return (
				<div key={key}>
					<hr/>
					<span id="repo1">{value.repository}</span> 
					<span id="repo2">{value.lang} &#9734; {value.star} &#9880;{value.vay}</span><br/><br/>
					<span id="update1">{value.updated}</span>
					
				</div>
			)
		})
	}

	

    render() {
    	
    return (
      
  <h3>
        {this._renderObject()}
     </h3>
    )
  }
    
}

export default FirstCompo22;