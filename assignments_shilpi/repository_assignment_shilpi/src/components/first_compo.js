import React,{ Component} from 'react';
import FirstCompo1 from './first_1_compo';
import FirstCompo2 from './first_2_compo';
class FirstCompo extends Component {
    
    render(){
        return (
        <div className="FirstCompo">
                <FirstCompo1 />
                <FirstCompo2 />
        </div>
        );
    }
}

export default FirstCompo;