import React, { Component } from 'react';
import TwitAccount from './TwitAccount';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TwitAccount/>
      </div>
    );
  }
}

export default App;