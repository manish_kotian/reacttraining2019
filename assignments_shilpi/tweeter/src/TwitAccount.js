import React,{ Component } from 'react';
import ProfileDetail from './profile/ProfileDetail';
import Twit from './twit/Twit';
import './style/Twit.css';

class TwitAccount extends Component
{
    constructor(props)
    {
        super(props);
    }

    render(){
        return(
            <div className = "TwitAccount">
                <ProfileDetail/>
                <Twit/>
            </div>
        )
    }
}
export default TwitAccount;