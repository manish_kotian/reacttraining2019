import React, { Component } from 'react';
import ProfileSocialDetail from './ProfileSocialDetail';
import profiletheme from '../Pic/background.png';
import '../style/Profile.css';

const ProfileTheme = (props)=> {
    return(
        <div className = "ProfileTheme">
            <img src = {props.theme}/>
        </div>
    )
}

class ProfileDetail extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            profiletheme: profiletheme
        }
    }

    render()
    {
        return (
            <div className = "ProfileDetail">
                <ProfileTheme theme = {this.state.profiletheme}/>
                <ProfileSocialDetail/>
            </div>
        )
    }
}
export default ProfileDetail;