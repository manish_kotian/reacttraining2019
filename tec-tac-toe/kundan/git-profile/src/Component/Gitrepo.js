import React from 'react';

const GitRepoList = (props) => {
    return (
        props.repolist.map((item,index) => {
           return(
                <div className = "RepoList " key = {index}>
                    <div>
                        <p className="Font-lg"><a href= "#">{item.reponame}</a></p>
                        <p>{item.updatedAt}</p>
                        
                    </div>
                    <div></div>
                    <div>
                        <p className="Font-lg">{item.stack}</p>
                    </div>
                </div>
           ) 
        })
    );
}
export default GitRepoList;