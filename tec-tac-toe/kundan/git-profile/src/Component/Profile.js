import React, {Component} from 'react';
import GitRepoList from './Gitrepo';
import ProfileSumary from './ProfileSummary';
import SearchRepo from './Search';
import "../global.css";
import profileImg from '../pic/index.png';

class Profile extends Component 
{
    profileDetail = {};
    oldrepolist = [];

    constructor(props)
    {
        super(props);  
        this.profileDetail = {
            name: 'kundan kumar',
            username: 'kundan8239',
            picUrl: profileImg,
            followers: 5,
            following: 39,
            repolist: [
                {"reponame": 'react-native', "stack": 'JavaScript', 'updatedAt': '3 Months before'},
                {"reponame": 'jest', "stack": 'C++', 'updatedAt': '7 Months before'},
                {"reponame": 'duckling', "stack": 'Java', 'updatedAt': '5 Months before'},
                {"reponame": 'fresco', "stack": 'React Js', 'updatedAt': '10 Months before'},
                {"reponame": 'react', "stack": 'Perl', 'updatedAt': '12 Months before'},
                {"reponame": 'osquery', "stack": 'Node js', 'updatedAt': '1 Months before'},
                {"reponame": 'yoga', "stack": 'React js', 'updatedAt': '15 days before'},
                {"reponame": 'folly', "stack": 'React js', 'updatedAt': '1 day before'},
            ],
            repo: 10,
        };
        this.oldrepolist = this.profileDetail.repolist;
        this.state = this.profileDetail;
    }

    onSearch = (event) => {
        var text = event.target.value;
        console.log(text.length);
        if(text.length > 2)
        {
            var updatedRepolist = this.profileDetail.repolist.filter((item) => {
                return item['reponame'].toLowerCase().search(text.toLowerCase()) !== -1;                  
            });
            this.profileDetail.repolist = updatedRepolist;
            this.setState({profile: this.profileDetail});
        }
        else
        {
            this.profileDetail.repolist = this.oldrepolist;
            this.setState({profile: this.profileDetail});
        }
    }

    render()
    {
        return(
            <div className = "Container">
                <div className = "Sidebar">
                    <div>
                        <ProfileSumary name = {this.state.name} username = {this.state.username} 
                         picurl = {this.state.picUrl} followers = {this.state.followers}
                         repo = {this.state.repo}   following = {this.state.following}
                         /> 
                    </div>
                </div>
                <div className = "ContentSection">
                    <SearchRepo onSearch = {this.onSearch}/>
                    <GitRepoList  repolist = {this.profileDetail.repolist}/>
                </div>
            </div>
        );
    }
}
export default Profile;