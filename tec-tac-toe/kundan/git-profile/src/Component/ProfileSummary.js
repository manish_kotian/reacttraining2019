import React from 'react';


const ProfilePic = (props) => {
    return (
        <div className = "ProfilePic">
            <img src = {props.picurl} />
        </div>
    );
}

const ProfileSumary = (props) => {
    return (
        <div>
            <ProfilePic picurl = {props.picurl}/> <br/><br/>
            <h2 className = "Text-align-left Font-lg">{props.name}</h2>
            <h2 className = "Text-align-left">{props.username}</h2>
            <hr/>
            <div className ="Repodetail Font-lg">
                <div>
                    <h3>{props.followers}</h3>
                    <h5>followers</h5>
                </div>
                <div>
                    <h3>{props.repo}</h3>
                    <h5>Repositories</h5>
                </div>
                <div>
                    <h3>{props.following}</h3>
                    <h5>following</h5>
                </div>
            </div>
            <hr/>
        </div>
    );
}
export default ProfileSumary;